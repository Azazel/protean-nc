use gdk::gdk_pixbuf;
use gtk::prelude::*;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{cell::RefCell, env, rc::Rc};
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
struct Notif {
    appname: String,
    body: String,
    icon: std::path::PathBuf,
    summary: String,
    timestamp: String,
    urgency: Urgency,
}
#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
enum Urgency {
    NORMAL,
    LOW,
    CRITICAL,
}
impl Default for Urgency {
    fn default() -> Self {
        Self::NORMAL
    }
}
struct RuntimeData {
    config_dir: String,
}
impl Notif {
    fn new(
        appname: String,
        body: String,
        icon: std::path::PathBuf,
        summary: String,
        timestamp: String,
        urgency: Urgency,
    ) -> Notif {
        Notif {
            appname,
            body,
            icon,
            summary,
            timestamp,
            urgency,
        }
    }
}
pub fn expand_env_vars(s: &str) -> String {
    match Regex::new(r"\$([[:word:]]*)") {
        Ok(x) => x
            .replace_all(s, |captures: &regex::Captures<'_>| match &captures[1] {
                "" => String::from(""),
                varname => {
                    println!("{}", varname);
                    env::var(varname).expect("Bad Var Name")
                }
            })
            .to_string(),
        Err(_e) => s.to_string(),
    }
}
fn exec(cmd: &str, args: &[&str]) -> String {
    let output = std::process::Command::new(cmd)
        .args(args)
        .output()
        .expect("failed to execute cmd");

    String::from_utf8(output.stdout).unwrap()
}

fn main() {
    let runtime_data: Rc<RefCell<RuntimeData>> = Rc::new(RefCell::new(RuntimeData {
        config_dir: "/home/arjun/.config/protean_nc/styles.css".to_owned(),
    }));

    gtk::init().unwrap();
    let file_path = "$HOME/.cache/dunst/notifications.json";
    let json_string = exec(
        "sh",
        &["-c", &("cat ".to_owned() + file_path + " | jq -s reverse ")],
    );
    // there is zero point to the refcell that was here btw
    let notifs: Vec<Notif> = serde_json::from_str(&json_string).unwrap();

    // println!("{:?}", notifs.borrow().len());
    // get only 100
    let notifs = Rc::new(RefCell::new(notifs[0..100].to_vec()));

    let provider = gtk::CssProvider::new();
    if let Err(why) =
        provider.load_from_path(&format!("{}/style.css", runtime_data.borrow().config_dir))
    {
        eprintln!("Failed to load custom CSS: {}", why);
        provider
            .load_from_data(include_bytes!("css/styles.css"))
            .unwrap();
    }

    let window = gtk::Window::new(gtk::WindowType::Toplevel);
    window.set_widget_name("window");
    // let screen = gtk_window_get_screen(window);
    let screen = gtk::prelude::GtkWindowExt::screen(&window).unwrap();
    gtk::StyleContext::add_provider_for_screen(
        &screen,
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    window.set_default_size(500, 500);
    gtk_layer_shell::init_for_window(&window);
    gtk_layer_shell::set_layer(&window, gtk_layer_shell::Layer::Overlay);
    gtk_layer_shell::set_anchor(&window, gtk_layer_shell::Edge::Top, true);
    /* gtk_layer_shell::set_anchor(&window, gtk_layer_shell::Edge::Bottom, true);
    gtk_layer_shell::set_anchor(&window, gtk_layer_shell::Edge::Left, true); */
    gtk_layer_shell::set_anchor(&window, gtk_layer_shell::Edge::Right, true);
    gtk_layer_shell::set_namespace(&window, "hello-world");
    gtk_layer_shell::set_keyboard_mode(&window, gtk_layer_shell::KeyboardMode::OnDemand);
    // ignore exclusive area
    gtk_layer_shell::set_exclusive_zone(&window, -1);
    // gtk_layer_shell::set_margin(&window, gtk_layer_shell::Edge::Top, 100);
    let notification_label = gtk::Label::builder()
        .hexpand(true)
        .name("heading-label")
        .label("Notifications")
        .build();
    let dnd_label = gtk::Label::builder()
        .name("dnd-label")
        .label("Do not Disturb")
        .hexpand(true)
        .build();

    window.connect_key_press_event(move |window, event| {
        use gtk::gdk::keys::constants;
        match event.keyval() {
            // Close window on escape
            constants::Escape => {
                window.close();
                // exit from gtk main loop
                gtk::main_quit();
                Inhibit(true)
            }
            _ => Inhibit(false),
        }
    });
    let fixed = gtk::Fixed::builder().build();
    let sw = gtk::ScrolledWindow::builder().height_request(500).build();
    sw.set_shadow_type(gtk::ShadowType::EtchedIn);
    sw.set_policy(gtk::PolicyType::Never, gtk::PolicyType::Automatic);
    let main_vbox = gtk::Box::builder()
        .orientation(gtk::Orientation::Vertical)
        .halign(gtk::Align::Center)
        .vexpand(false)
        .width_request(300)
        .height_request(50)
        .name("Notification")
        .build();
    let header_row = gtk::Box::builder()
        .orientation(gtk::Orientation::Horizontal)
        .name("header")
        .build();
    let dnd_row = gtk::Box::builder()
        .orientation(gtk::Orientation::Horizontal)
        .name("dnd-row")
        .build();
    let clear_button = gtk::Button::builder()
        .label("Clear All")
        .halign(gtk::Align::Center)
        .height_request(50)
        .name("notifications-clear-btn")
        .build();
    clear_button.connect_clicked(|_| {
        println!("clear all");
    });

    let dnd_toggle_button = gtk::Switch::builder().halign(gtk::Align::End).build();
    // for every entry in the notifications array, create a widget and collect it
    let notif_widgets = gtk::Box::builder()
        .orientation(gtk::Orientation::Vertical)
        .name("notifications-list")
        .build();

    for i in 0..notifs.borrow().len() {
        let notif = notifs.borrow()[i].clone();

        let notification_box = gtk::Box::builder()
            .orientation(gtk::Orientation::Horizontal)
            .margin_top(35)
            .build();
        let notif_content_box = gtk::Box::builder()
            .orientation(gtk::Orientation::Vertical)
            .halign(gtk::Align::Center)
            .width_request(100)
            .build();
        let appname_label = gtk::Label::builder()
            .label(&notif.appname)
            .width_request(70)
            .halign(gtk::Align::Center)
            .max_width_chars(20)
            .wrap(true)
            .build();
        gtk::StyleContext::add_class(&appname_label.style_context(), "notif-appname");

        let summary_label = gtk::Label::builder()
            .label(&notif.summary)
            .hexpand(true)
            .width_request(40)
            .max_width_chars(20)
            .wrap(true)
            .build();
        gtk::StyleContext::add_class(&summary_label.style_context(), "notfi-summary");

        let body_label = gtk::Label::builder()
            .label(&notif.body)
            .name("notif-body")
            .width_request(40)
            .max_width_chars(20)
            .wrap(true)
            .build();
        gtk::StyleContext::add_class(&body_label.style_context(), "notfi-body");

        let timestamp_label = gtk::Label::builder()
            .label(&notif.timestamp)
            .width_request(40)
            .wrap(true)
            .halign(gtk::Align::End)
            .valign(gtk::Align::Start)
            .build();
        gtk::StyleContext::add_class(&timestamp_label.style_context(), "notif-time");

        let notif_icon_string: String = if notif.icon.exists() {
            notif.icon.to_str().unwrap().to_owned()
        } else {
            std::string::String::from("")
        };

        let icon_path = match notif_icon_string == "" {
            true => {
                std::fs::canonicalize(std::path::PathBuf::from("./src/assets/notification.svg"))
                    .unwrap()
                    .into_os_string()
                    .into_string()
                    .unwrap()
            }

            false => notif_icon_string,
        };

        // println!("{}", icon_path);
        let notif_icon_pixbuf =
            gdk_pixbuf::Pixbuf::from_file_at_scale(&icon_path, 32, 32, true).unwrap();

        let notif_icon = gtk::Image::from_pixbuf(Some(&notif_icon_pixbuf));
        notif_icon.set_valign(gtk::Align::Start);
        gtk::StyleContext::add_class(&notif_icon.style_context(), "notif-icon");
        let close_btn = gtk::Button::builder()
            .halign(gtk::Align::End)
            .height_request(3)
            .build();
        //on click delete ith notification from the array
        close_btn.connect_clicked({
            let notifs = notifs.clone();
            move |_| {
                // remove the ith notification from the array
                notifs.borrow_mut().remove(i);
            }
        });

        // add the cross icon to the button
        let close_btn_icon = gtk::Image::from_icon_name(Some("window-close"), gtk::IconSize::Menu);
        close_btn.add(&close_btn_icon);
        gtk::StyleContext::add_class(&close_btn.style_context(), "notif-close");
        gtk::StyleContext::add_class(&close_btn_icon.style_context(), "notif-close-icon");

        notification_box.add(&notif_icon);
        notification_box.add(&notif_content_box);
        notif_content_box.add(&appname_label);
        notif_content_box.add(&summary_label);
        notif_content_box.add(&body_label);
        notification_box.add(&timestamp_label);
        notification_box.add(&close_btn);
        notif_widgets.add(&notification_box);
    }
    println!("done with notifications");

    header_row.add(&notification_label);
    header_row.add(&clear_button);
    dnd_row.add(&dnd_label);
    dnd_row.add(&dnd_toggle_button);

    main_vbox.add(&header_row);
    main_vbox.add(&dnd_row);
    main_vbox.add(&sw);
    sw.add(&notif_widgets);
    fixed.put(&main_vbox, 0, 20);
    window.add(&fixed);
    window.show_all();
    gtk::main();
}
